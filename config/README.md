# Simulation Config classes

The config folder and namespace is for project behaviors that are
specific to individual simulation codes (ACE3P, OpenFOAM, Truchas,
etc.). The code in this namespace should be used on an interim
basis; the long term plan is to implement all simulation-specific
behavior in python operators located in the simulation-workflows
repository.
