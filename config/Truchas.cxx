//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "Truchas.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/common/UUID.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Properties.h"
#include "smtk/resource/Resource.h"

#include "boost/filesystem.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace config
{
/// Return the filename of the export operator
bool Truchas::exportOpFilename(std::string& name) const
{
  name = "Truchas.py";
  return true;
}

bool Truchas::postCreate(smtk::project::ProjectPtr project) const
{
#ifndef NDEBUG
  std::cout << "Enter Truchas::postCreate()" << std::endl;
#endif

  // Relabel model entities to include pedigree id
  // This is an interim hack until Resource Panel can display pedigree ids
  auto modelResource = project->findResource<smtk::model::Resource>("default");
  if (modelResource)
  {
    std::cout << "Renaming model volumes" << std::endl;
    this->renameModelEntities(modelResource, smtk::model::VOLUME, "block ");

    std::cout << "Renaming model faces" << std::endl;
    this->renameModelEntities(modelResource, smtk::model::FACE, "side set ");

    // Mark the resource as modified.
    modelResource->setClean(false);
  } // if (modelResource)

  // If second mesh was loaded, assign to induction heating component item
  this->assignAltModelResource(project, true);

  return true;
}

bool Truchas::postImportModel(smtk::project::ProjectPtr project, const std::string& role) const
{
  if (role != "second")
  {
    std::cerr << "Unrecognized role" << role << std::endl;
    return false;
  }

  this->assignAltModelResource(project);
  return true;
}

bool Truchas::preExport(
  smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
{
  int paramVersion = exportOp->parameters()->definition()->version();
  switch (paramVersion)
  {
    case 0:
    {
      auto projectNameItem = exportOp->parameters()->findString("ProjectName");
      if (projectNameItem)
      {
        projectNameItem->setValue(project->name());
      }
      return true;
    }
    break;

    case 1:
      if (!this->preExportV1(project, exportOp))
      {
        return false;
      }
      break;

    case 2:
      if (!this->preExportV2(project, exportOp))
      {
        return false;
      }
      break;

    default:
      std::cerr << "Unsupported parameters version " << paramVersion;
      return false;
      break;
  }

  // Common to versions 1, 2:

  // Attribute Resource
  auto attResource = project->findResource<smtk::attribute::Resource>("default");
  auto attResourceItem = exportOp->parameters()->findResource("attributes");
  if (attResourceItem)
  {
    attResourceItem->setValue(attResource);
  }

  // Mesh file
  auto meshFileItem = exportOp->parameters()->findFile("mesh-file");
  this->setModelFile(meshFileItem, project, "default", exportOp->log());

  // Altmesh file (when induction heating enabled)
  auto altMeshFileItem = exportOp->parameters()->findFile("alt-mesh-file");
  auto simatts = project->findResource<smtk::attribute::Resource>("default");
  if (this->isInductionHeatingEnabled(simatts))
  {
    this->setModelFile(altMeshFileItem, project, "second", exportOp->log());
  }

  return true;
}

void Truchas::tagResources(smtk::project::ProjectPtr project) const
{
#ifndef NDEBUG
  std::cout << "Enter Truchas::tagResources()" << std::endl;
#endif

  // Sorry for the magic strings; at some point this code should be
  // migrated to the truchas-extension project, and preferably implemented
  // in a python operation.

  // Add a string property to each model to indicate its use in the project
  auto firstModelRes = project->findResource<smtk::model::Resource>("default");
  if (firstModelRes != nullptr)
  {
    firstModelRes->properties().get<std::string>()["project.use"] = "heat-transfer";
  }

  auto secondModelRes = project->findResource<smtk::model::Resource>("second");
  if (secondModelRes != nullptr)
  {
    secondModelRes->properties().get<std::string>()["project.use"] = "induction-heating";
  }
}

void Truchas::assignAltModelResource(smtk::project::ProjectPtr project, bool okToCreateAtt) const
{
  // If second mesh was loaded, assign to induction heating component item
  auto altModelResource = project->findResource<smtk::model::Resource>("second");
  if (altModelResource == nullptr)
  {
    return;
  }

  auto attResource = project->findResource<smtk::attribute::Resource>("default");
  if (attResource == nullptr)
  {
    return;
  }

  const std::string emName = "electromagnetics";
  auto emAtt = attResource->findAttribute(emName);
  if (emAtt == nullptr && okToCreateAtt)
  {
    emAtt = attResource->createAttribute(emName, emName);
  }

  if (emAtt == nullptr)
  {
    return;
  }

  auto modelItem = emAtt->findComponent("model");
  if (modelItem)
  {
    // Get model entity(ies)
    auto uuids = altModelResource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY, true);
    if (uuids.size() != 1)
    {
      std::cerr << "Unexpected number of models in " << altModelResource->location()
                << ", expected 1, got " << uuids.size() << std::endl;
    }
    else
    {
      auto uuid = *(uuids.begin());
      auto model = altModelResource->find(uuid);
      if (model)
      {
        altModelResource->setIntegerProperty(uuid, emName, 1);
        modelItem->setValue(model);
      }
    } // if (1 model)
  }   // if (modelItem)
}

bool Truchas::isInductionHeatingEnabled(smtk::attribute::ResourcePtr& attResource) const
{
  // Check attribute resource entities
  if (attResource == nullptr)
  {
    return false;
  }

  // Get "analysis" att
  auto analysisAtt = attResource->findAttribute("analysis");
  if (analysisAtt == nullptr)
  {
    return false;
  }

  // Get "Heat Transfer" item
  auto htItem = analysisAtt->findGroup("Heat Transfer");
  if (htItem == nullptr)
  {
    return false;
  }

  // Get "Induction Heating" item
  auto ihItem = htItem->find("Induction Heating");
  if (ihItem == nullptr)
  {
    return false;
  }

  return ihItem->isEnabled();
}

bool Truchas::preExportV1(
  smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
{
  // Model
  auto modelItem = exportOp->parameters()->findComponent("model");
  if (modelItem)
  {
    auto modelResource = project->findResource<smtk::model::Resource>("default");
    if (modelResource == nullptr)
    {
      smtkErrorMacro(exportOp->log(), "Project has no \"default\" model resource");
      return false;
    }

    // Get all models in model resource
    auto uuids = modelResource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY, true);
    std::vector<smtk::resource::ComponentPtr> modelList;
    for (const auto& uuid : uuids)
    {
      auto model = modelResource->find(uuid);
      if (model)
      {
        modelList.push_back(model);
      }
    } // for

    if (modelList.size() == 1)
    {
      modelItem->setValue(modelList[0]);
    }
    else
    {
      smtkErrorMacro(exportOp->log(), "Unable to assign model because"
                                      " the number of models in the project is "
          << modelList.size());
      smtkErrorMacro(exportOp->log(), "Unable to find model resource - cannot export");
      return false;
    }
  }

  // Output file
  auto inpFileItem = exportOp->parameters()->findFile("output-file");
  if (inpFileItem)
  {
    boost::filesystem::path projectDir(project->directory());
    std::string inpFileName = project->name() + ".inp";
    boost::filesystem::path inpFilePath = projectDir / "export" / inpFileName;
    inpFileItem->setValue(0, inpFilePath.string());
  }

  return true;
}

bool Truchas::preExportV2(
  smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
{
  boost::filesystem::path projectDir(project->directory());
  boost::filesystem::path exportDir = projectDir / "export";
  std::string inpFileName = project->name() + ".inp";

  // Model
  auto modelItem = exportOp->parameters()->findResource("model");
  if (modelItem)
  {
    auto modelResource = project->findResource<smtk::model::Resource>("default");
    if (modelResource == nullptr)
    {
      smtkErrorMacro(exportOp->log(), "Unable to find model resource - cannot export");
      return false;
    }
    modelItem->setValue(modelResource);
  }

  // Attributes
  auto attResource = project->findResource<smtk::attribute::Resource>("default");
  auto attResourceItem = exportOp->parameters()->findResource("attributes");
  if (attResourceItem)
  {
    attResourceItem->setValue(attResource);
  }

  // Analysis Code - not implemented in earler versions
  smtk::attribute::StringItemPtr expAnalysisCodeItem =
    exportOp->parameters()->findString("analysis-code");
  if (expAnalysisCodeItem == nullptr)
  {
    smtkErrorMacro(exportOp->log(), "Internal error: unable to find \"analysis-code\" item");
    return false;
  }

  auto simAnalysisAtt = attResource->findAttribute("analysis");
  if (simAnalysisAtt == nullptr)
  {
    smtkErrorMacro(exportOp->log(), "Internal error: unable to find analysis attribute");
    return false;
  }

  // Genre update introduces "Analysis" item
  auto simAnalysisItem = simAnalysisAtt->findString("Analysis");
  if (simAnalysisItem == nullptr)
  {
    // Configure for legacy analysis attribute
    if (!expAnalysisCodeItem->setValue("Truchas"))
    {
      smtkWarningMacro(exportOp->log(), "Internal error: unable to set analysis code to Truchas");
      return false;
    }

    auto inpFileItem = expAnalysisCodeItem->findAs<smtk::attribute::FileItem>(
      "output-file", smtk::attribute::SearchStyle::RECURSIVE);
    if (inpFileItem == nullptr)
    {
      smtkErrorMacro(exportOp->log(), "Internal error: unable to find \"output-file\" item");
      return false;
    }
    boost::filesystem::path inpFilePath = projectDir / "export" / inpFileName;
    inpFileItem->setValue(0, inpFilePath.string());
    return true;
  }

  // Continue with current analysis attributge
  if (!simAnalysisItem->isSet())
  {
    smtkErrorMacro(exportOp->log(), "No Analysis specified in the simulation attributes");
    return false;
  }

  // Check for analysis code item
  std::string simAnalysisCode = simAnalysisItem->value();
  if (!expAnalysisCodeItem->setValue(simAnalysisCode))
  {
    smtkWarningMacro(
      exportOp->log(), "Internal error: unable to set analysis code " << simAnalysisCode);
    return false;
  }

  // Output folder
  auto folderItem = expAnalysisCodeItem->findAs<smtk::attribute::DirectoryItem>(
    "output-folder", smtk::attribute::SearchStyle::RECURSIVE);
  if (folderItem == nullptr)
  {
    smtkErrorMacro(exportOp->log(), "Internal error: unable to find \"output-folder\" item");
    return false;
  }
  folderItem->setValue(exportDir.string());

  // Output file
  auto inpFileItem = expAnalysisCodeItem->findAs<smtk::attribute::FileItem>(
    "output-file", smtk::attribute::SearchStyle::RECURSIVE);
  if (inpFileItem == nullptr)
  {
    smtkErrorMacro(exportOp->log(), "Internal error: unable to find \"output-file\" item");
    return false;
  }

  boost::filesystem::path inpFilePath = projectDir / "export" / inpFileName;
  inpFileItem->setValue(0, inpFilePath.string());

  return true;
}

void Truchas::renameModelEntities(smtk::model::ResourcePtr modelResource,
  smtk::model::EntityTypeBits entType, const std::string& prefix) const
{
  auto entRefs = modelResource->findEntitiesOfType(entType, true);

  // First pass - find the max id
  int maxId = -1;
  for (auto entRef : entRefs)
  {
    auto uuid = entRef.entity();
    auto propList = modelResource->integerProperty(uuid, "pedigree id");
    if (propList.size() == 1)
    {
      int id = propList[0];
      maxId = id > maxId ? id : maxId;
    }
  } // for

  // Sanity check
  if (maxId < 0)
  {
    std::cerr << "No pedigree ids found" << std::endl;
    return;
  }

  // Get number of digits
  int number = maxId;
  int digits = 0;
  while (number)
  {
    number /= 10;
    digits++;
  }

  // Second pass - update entity names
  std::stringstream ss;
  for (auto entRef : entRefs)
  {
    auto uuid = entRef.entity();
    auto propList = modelResource->integerProperty(uuid, "pedigree id");
    if (propList.size() == 1)
    {
      int id = propList[0];
      ss.str("");
      ss << prefix << std::setfill('0') << std::setw(digits) << id << " - " << entRef.name();
      entRef.setName(ss.str());
    } // if
  }   // for (entRefs)
}

} // namespace config
