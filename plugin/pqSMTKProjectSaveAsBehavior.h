//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectSaveAsBehavior_h
#define pqSMTKProjectSaveAsBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to a new/different
/// folder in the filesystem.
class pqProjectSaveAsReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectSaveAsReaction(QAction* parent);

  void saveAsProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->saveAsProject(); }

private:
  Q_DISABLE_COPY(pqProjectSaveAsReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectSaveAsBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectSaveAsBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectSaveAsBehavior() override;

protected:
  pqSMTKProjectSaveAsBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectSaveAsBehavior);
};

#endif
