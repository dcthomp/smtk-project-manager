//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectSaveAsBehavior.h"

#include "pqSMTKProjectLoader.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// ParaView client
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqRecentlyUsedResourcesList.h"
#include "pqServer.h"
#include "pqServerResource.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>
#include <QString>
#include <QTextStream>

//-----------------------------------------------------------------------------
pqProjectSaveAsReaction::pqProjectSaveAsReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectSaveAsReaction::saveAsProject()
{
  // Access the active server and project manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto& logger = smtk::io::Logger::instance();

  // Check if current project is modified
  auto currentProject = projectManager->getCurrentProject();
  if ((currentProject != nullptr) && (!currentProject->clean()))
  {
    QMessageBox msgBox(pqCoreUtilities::mainWidget());
    msgBox.setText("The current project has modifications.");
    msgBox.setInformativeText("Do you want to save the current project first?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Yes);

    int ret = msgBox.exec();
    if (ret == QMessageBox::Cancel)
    {
      return;
    }

    if (ret == QMessageBox::Yes)
    {
      bool saved = projectManager->saveProject(logger);
      if (!saved)
      {
        QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed To Save Project"),
          tr(logger.convertToString().c_str()));
        return;
      }
    }
  }

  // Get workspace folder from smtk settings
  std::string projectsRootFolder;
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* prop = proxy->GetProperty("ProjectsRootFolder");
    auto stringProp = vtkSMStringVectorProperty::SafeDownCast(prop);
    if (stringProp)
    {
      projectsRootFolder = stringProp->GetElement(0);
    } // if (stringProp)
  }   // if (proxy)

  // Construct a file dialog for the user to select the project directory to load
  pqFileDialog fileDialog(server, pqCoreUtilities::mainWidget(), tr("Select New Project Directory"),
    QString::fromStdString(projectsRootFolder));
  fileDialog.setObjectName("FileSaveAsDialog");
  fileDialog.setFileMode(pqFileDialog::Directory);
  fileDialog.setShowHidden(true);

  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString directory = fileDialog.getSelectedFiles()[0];

  // Call the saveAsProject() method
  auto newProject = projectManager->saveAsProject(directory.toStdString(), logger);
  if (newProject == nullptr)
  {
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed To SaveAs Project"),
      tr(logger.convertToString().c_str()));
    return;
  }

  // Add new project to the recently-used list
  pqServerResource resource = server->getResource();
  resource.addData(RECENTLY_USED_PROJECTS_TAG, "1");
  resource.setPath(directory);

  pqRecentlyUsedResourcesList& mruList = pqApplicationCore::instance()->recentlyUsedResources();
  mruList.add(resource);
  mruList.save(*pqApplicationCore::instance()->settings());

  // Pop up confirmation message
  QString msg;
  QTextStream qs(&msg);
  qs << "Saved project to new folder" << directory;
  qInfo() << msg;

  QMessageBox::information(pqCoreUtilities::mainWidget(), "Success", msg);
} // saveAsProject()

//-----------------------------------------------------------------------------
static pqSMTKProjectSaveAsBehavior* g_instance = nullptr;

pqSMTKProjectSaveAsBehavior::pqSMTKProjectSaveAsBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectSaveAsBehavior* pqSMTKProjectSaveAsBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectSaveAsBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectSaveAsBehavior::~pqSMTKProjectSaveAsBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
